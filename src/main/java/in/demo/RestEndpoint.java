package in.demo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/search")
public class RestEndpoint {

    @Autowired
    private DelegateService delegateService;

    @PostConstruct
    public void init() {
        log.info("RestEndpoint initiated..");
    }

    @GetMapping("cacheable/{id}")
    public ResponseEntity<Person> find(@PathVariable("id") Integer id) throws InterruptedException {
        return ResponseEntity.ok(delegateService.find(id));
    }

    @GetMapping("realtime/{id}")
    public ResponseEntity<Person> findRealtime(@PathVariable("id") Integer id) throws InterruptedException {
        return ResponseEntity.ok(delegateService.findRealtime(id));
    }
}
