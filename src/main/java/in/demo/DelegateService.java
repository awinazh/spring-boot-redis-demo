package in.demo;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DelegateService {

    @Cacheable("persons")
    public Person find(Integer id) throws InterruptedException {
        return findRealtime(id);
    }

    public Person findRealtime(Integer id) throws InterruptedException {
        log.info(String.format("Returning realtime value for %s", id));
        Thread.sleep(1000);
        return Person
                .builder()
                .id(id)
                .name(String.format("Name(%s)", id))
                .build();
    }
}
