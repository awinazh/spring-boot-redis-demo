# Spring Cache

Caching is one of frequent ask for infrequently changed data to enhance the performance of any applicaton. Default spring in-memory cache doesn't provide TTL option for cached object, to overcome this situation Redis can be used with Spring Cache.

---

## Pre-requisite to run demo locally

1. Docker
2. OpenJDK8
3. Git
4. Maven


## Setup redis service

To run this demo app you can run redis server locally using docker image. Run following commands.

1. Download docker image: `docker pull redis`
2. Run docker container: `docker run --rm -d -p 6379:6379 --name redis redis`
3. Stop docker container: `docker stop redis`

---

## Clone a repository

Next, clone repo to local environment.

`git clone git@bitbucket.org:awinazh/spring-boot-redis-demo.git`

---

## Run spring-boot application

1. Go to the directory where you clone repository
2. Run mvn command: mvn springboot:run

---

## Demo caching

1. Open any rest client
2. Hit uri repeatedly: `localhost:8080/search/cacheable/1`
3. Hit uri repeatedly: `localhost:8080/search/realtime/2`
4. Check the console for logs..